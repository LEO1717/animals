package com.company;

/**
 * класс собак
 *
 * Created by LEONID on 13.10.2014.
 */
public class Dog implements animals{

    @Override
    public String getName() {
        return "Собака";
    }

    @Override
    public String getVoice() {
        return "гав-гав";
    }
}
