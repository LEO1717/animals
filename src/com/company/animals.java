package com.company;

/**
 * Интерфейс,содержит в себе общие критерии животного
 * Created by LEONID on 13.10.2014.
 */
public interface animals {
    /**
     * Метод, возвращает название животного
     * @return (String) название животного
     */
    String getName();

    /**
     * Метод, возвращает голос (слова/говор)животного.
     * @return (String) голос (слова/говор) животного.
     */

    String getVoice();
}