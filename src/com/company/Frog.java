package com.company;

/**
 * класс лягушек
 *
 * Created by LEONID on 14.10.2014.
 */
public class Frog implements animals{

    @Override
    public String getName() {
        return "Лягушка";
    }

    @Override
    public String getVoice() {
        return "Ква-Ква";
    }
}
