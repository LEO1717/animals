package com.company;

public class Main {

    public static void main(String[] args) {
        Cat cat = new Cat();
        Dog dog = new Dog();
        Frog frog = new Frog();
        Cow cow = new Cow();
        Chicken chicken = new Chicken();
        Pig pig = new Pig();

        print(cat);
        print(dog);
        print(frog);
        print(cow);
        print(chicken);
        print(pig);
    }

    public static void print(animals animals){
        System.out.println(animals.getName()+
        " говорит " + animals.getVoice());
    }
}
